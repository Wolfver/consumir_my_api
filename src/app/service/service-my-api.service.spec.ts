import { TestBed } from '@angular/core/testing';
import { ServiceMyAPIService } from './service-my-api.service';

describe('ServiceMyAPIService', () => {
  let service: ServiceMyAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceMyAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
