import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceMyAPIService {

  public URI = 'http://localhost:3900/obtener/personal';

  constructor(private _http:HttpClient) { }

  getPersonal(){
    // const url = `${this.URI} `;

    return this._http.get(this.URI)
  }
}
