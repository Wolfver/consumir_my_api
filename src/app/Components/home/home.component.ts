import { Component, OnInit } from '@angular/core';
import { ServiceMyAPIService } from 'src/app/service/service-my-api.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  public personalRefac : any[] = []

  constructor(public _service:ServiceMyAPIService){
    this.getPersonalRefac();
  }

  ngOnInit(): void {
      this.getPersonalRefac();
  }
  
  getPersonalRefac(){
    this.personalRefac = [];
    this._service.getPersonal()
    .subscribe((data:any)=>{

      console.log(data);

      this.personalRefac = data.result;
      console.log (this.personalRefac);
      
    });

  }

}